<?php

namespace Solokod\SoloKasa;

class Payment
{
    private $adminURL;

    public function __construct()
    {
        add_action('admin_post_addPayment', [$this, 'addPayment']);
        add_action('admin_post_editPayment', [$this, 'editPayment']);
        add_action('admin_post_listPayment', [$this, 'listPayment']);
        add_action("admin_post_delPayment", [$this, "delPayment"]);
        add_action('admin_post_getPayment', [$this, 'getPayment']);
        add_action('admin_post_getPaymentInfo', [$this, 'getPaymentInfo']);

        $this->adminURL = admin_url('admin-post.php');
    }

    public function getPaymentInfo()
    {
        global $wpdb;

        $currency = $_GET['currency'];

        $rates = $this->tcmbGetRates();
        $usd = $rates[0];
        $eur = $rates[1];

        $table = $wpdb->prefix . "kasa_payments";

        $gelir = $wpdb->get_var("SELECT SUM(total) FROM $table WHERE payment_type = 'gelir'");
        $gider = $wpdb->get_var("SELECT SUM(total) FROM $table WHERE payment_type = 'gider'");
        $symbol = '&#8378;';

        switch ($currency) {
            case 'dolar':
                $gelir /= $usd;
                $gider /= $usd;
                $symbol = '&#36;';
                break;

            case 'euro':
                $gelir /= $eur;
                $gider /= $eur;
                $symbol = '&#8364;';
                break;

            default:
                $symbol = '&#8378;';
        }

        $gelir = number_format($gelir, 2, ',', '.');
        $gider = number_format($gider, 2, ',', '.');

        $html = <<<HTML
            <div class="infoBoxIncome">
                <div class="title">Toplam Gelir</div>
                <div class="amount">$symbol$gelir</div>
            </div>
            <div class="infoBoxExpense">
                <div class="title">Toplam Gider</div>
                <div class="amount">$symbol$gider</div>
            </div>
        HTML;

        echo $html;
    }

    public function editPayment()
    {
        $paymentID = $_GET["id"];

        $total = str_replace(",", ".", $_POST["total"]);
        $remainder = $_POST["remainder"] == "" ? "0" : $_POST["remainder"];

        $data = array(
            'paid_date' => $_POST["date"],
            "customer" => $_POST["customer"],
            "description" => $_POST["description"],
            "payment_type" => $_POST["type"],
            "status" => $_POST["status"],
            "total" => $total,
            "remainder" => $remainder
        );
        $format = array('%s', '%s', '%s', '%s', '%s', '%.2f', '%.2f');
        $where = array('id' => $paymentID);

        global $wpdb;
        $table = $wpdb->prefix . "kasa_payments";
        $wpdb->update($table, $data, $where, $format);

        header("HX-Redirect: /wp-admin/admin.php?page=kasa");
    }

    public function getPayment($payment_id)
    {
        $paymentID = $_GET["id"];

        global $wpdb;

        $table = $wpdb->prefix . "kasa_payments";
        $row = $wpdb->get_row("SELECT * FROM $table WHERE id = " . $paymentID);

        $paidDate = substr($row->paid_date, 0, 10);

        $form = <<<HTML
            <form hx-post="$this->adminURL?action=editPayment&id=$paymentID" hx-trigger="submit" x-data="{ status: '$row->status', selectType: '$row->payment_type' }">
                <div class="formRow">
                    <label for="type">İşlem Türü</label>
                    <select name="type" id="type" x-model="selectType">
                        <option value="gelir">Gelir</option>    
                        <option value="gider">Gider</option>    
                    </select>
                </div>
                <div class="formRow">
                    <label for="customer">Müşteri</label>
                    <input name="customer" id="customer" type="text" value="$row->customer" required>
                </div>
                <div class="formRow">
                    <label for="date">Tarih</label>
                    <input name="date" id="date" type="date" value="$paidDate">
                </div>
                <div class="formRow">
                    <label for="description">Açıklama</label>
                    <textarea rows="3" name="description" id="description">$row->description</textarea>
                </div>
                <div class="formRow">
                    <label for="status">Durum</label>
                    <select name="status" id="status" x-model="status">
                        <option value="bekliyor">Bekliyor</option>    
                        <option value="kismi-odeme">Kısmi Ödendi</option>    
                        <option value="odendi">Ödendi</option>    
                    </select>
                </div>
                <div class="formRow">
                    <label for="total">Tutar</label>
                    <input name="total" id="total" type="text" value="$row->total" required>
                </div>
                <div class="formRow" x-show="status == 'kismi-odeme'" x-cloak>
                    <label for="remainder">Kalan</label>
                    <input name="remainder" id="remainder" type="text" value="$row->remainder">
                </div>
                <div class="formRow" style="margin-top: 24px">
                    <label></label>
                    <input type="submit" name="submit" id="submit" class="button button-primary" value="Bilgileri Düzenle">
                </div>
            </form>
        HTML;

        echo $form;
    }

    public function listPayment()
    {
        global $wpdb;


        $page = empty($_GET["page"]) ? 0 : ($_GET["page"]);

        $limit = 10;
        $offset = $page * $limit;

        $table = $wpdb->prefix . "kasa_payments";
        $results = $wpdb->get_results("SELECT * FROM $table ORDER BY paid_date DESC LIMIT $limit OFFSET $offset");

        $table = '<tr><td colspan="7" style="text-align: center">Loading...</td></tr>';

        $table = '';
        if (empty($results) && $page == 0) {
            $table = '<tr><td colspan="7" style="text-align: center">Henüz hiç hareket eklememiş gibi görünüyorsunuz!</td></tr>';
        }

        foreach ($results as $d) {
            $date = date_create($d->paid_date);
            $paidDate = date_format($date, 'd.m.Y');

            $type = $d->payment_type == "gider" ? "Gider" : "Gelir";
            $remainder = $d->remainder != "0" ? "(" . $d->remainder . ")" : "";

            $table .= '
                <tr>
                    <td>' . $paidDate . '</td>
                    <td>' . $d->customer . '</td>
                    <td>' . $d->description . '</td>
                    <td>' . $type . '</td>
                    <td>' . $this->getStatus($d->status) . '</td>
                    <td style="text-align:right">' . number_format($d->total, 2, ',', '.') . " " . $remainder . '</td>
                    <td>
                        <a href="/wp-admin/admin.php?page=kasa&module=edit&id=' . $d->id . '">Düzenle</a>
                        |
                        <a href="#" 
                            hx-delete="' . $this->adminURL . '?action=delPayment&id=' . $d->id . '"
                            hx-confirm="Are you sure you wish to delete your account?">
                            Sil
                        </a>
                    </td>
                </tr>';
        }

        if (!empty($results)) {
            $table .= '
                <tr id="LoadMoreRecord">
                    <td colspan="7" style="text-align: center">
                        <a href="#" 
                            hx-get="' . $this->adminURL . '?action=listPayment&page=' . ($page + 1) . '"
                            class="button button-secondary"> 
                            Hareketleri Getir
                        </a>
                    </td>
                </tr>';
        }

        echo $table;
    }

    public function addPayment()
    {
        global $wpdb;

        $table = $wpdb->prefix . "kasa_payments";

        $total = str_replace(",", ".", $_POST["total"]);
        $remainder = $_POST["remainder"] == "" ? "0" : $_POST["remainder"];

        $values = array(
            'paid_date' => $_POST["date"],
            "customer" => $_POST["customer"],
            "description" => $_POST["description"],
            "payment_type" => $_POST["type"],
            "status" => $_POST["status"],
            "total" => $total,
            "remainder" => $remainder
        );
        $result = $wpdb->insert($table, $values);
        error_log(print_r($result, true));

        header("HX-Redirect: /wp-admin/admin.php?page=kasa");
    }

    public function delPayment()
    {
        $paymentID = $_GET["id"];

        if ($paymentID > 0) {

            global $wpdb;

            $table = $wpdb->prefix . "kasa_payments";
            $delete = $wpdb->query("DELETE FROM $table WHERE id = $paymentID");

            //error_log(print_r($delete, true));
        }

        echo "";
    }

    private function getStatus(string $status): string
    {
        switch ($status) {
            case "bekliyor":
                return "Bekliyor";

            case "odendi":
                return "Ödendi";

            case "kismi-odeme":
                return "Kısmi Ödendi";

            default:
                return "Unknown";
        }
    }

    function tcmbGetRates(): array
    {
        $url = 'https://www.tcmb.gov.tr/kurlar/today.xml';
        $xml = simplexml_load_file($url);

        if ($xml === false) {
            return [];
        }

        $usd = $xml->Currency[0]->BanknoteSelling;
        $eur = $xml->Currency[3]->BanknoteSelling;

        return [$usd, $eur];
    }
}
