<?php

namespace Solokod\SoloKasa;

class Startup
{
    public function __construct()
    {
        add_action('admin_menu', [$this, 'registerMenu']);
        add_action('admin_enqueue_scripts', [$this, 'registerAssets']);
    }

    public function registerAssets(): void
    {
        wp_enqueue_style("solokasa", SOLOKASA_URL . "assets/css/solokasa.css?v=" . time());
        wp_enqueue_script('main-script', SOLOKASA_URL . "assets/js/main.js", array(), time(), true);
        wp_enqueue_script('htmx-script', 'https://unpkg.com/htmx.org@1.9.12/dist/htmx.min.js', array(), '1.9.12', true);
        wp_enqueue_script('alpine-script', 'https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js', array(), '3.14.0', true);
    }

    public function registerMenu(): void
    {
        $pageTitle = __("Kasa", "solokasa");
        $menuTitle = __("Kasa", "solokasa");
        $capability = "manage_options";
        $menuSlug = "kasa";
        $callback = [$this, "registerBasePage"];
        $iconURL = "dashicons-vault";
        $position = "25";

        add_menu_page($pageTitle, $menuTitle, $capability, $menuSlug, $callback, $iconURL, $position);
    }

    public function registerBasePage(): void
    {
        $version = Helper::getVersion();
        require SOLOKASA_PATH . 'templates/base.php';
    }
}
