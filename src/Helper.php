<?php
namespace Solokod\SoloKasa;

class Helper
{
    public static function getVersion(): string
    {
        $file = plugin_dir_path(dirname(__FILE__, 1)) . 'solokasa.php';
        $pluginData = get_plugin_data($file);
        return $pluginData['Version'];
    }
}