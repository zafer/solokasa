<?php
/**
 * Plugin Name:       SoloKasa
 * Description:       Minimalist gelir-gider takip eklentisi
 * Requires at least: 5.8
 * Requires PHP:      8.0
 * Version:           1.1.4
 * Author:            Solokod
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       solokasa
 */

use Solokod\SoloKasa\Payment;
use Solokod\SoloKasa\Startup;

defined("ABSPATH") || exit;

define("SOLOKASA_PATH", plugin_dir_path(__FILE__));
define("SOLOKASA_URL", plugin_dir_url(__FILE__));
 
require_once dirname(__FILE__) . "/vendor/autoload.php";

new Startup();
new Payment();

register_activation_hook(__FILE__, "solokasa_createDatabaseTable");
function solokasa_createDatabaseTable()
{
    global $wpdb;
    global $kasa_db_version;

    $tableName = $wpdb->prefix . 'kasa_payments';

    $charset_collate = $wpdb->get_charset_collate();

    if ($wpdb->get_var("show tables like '$tableName'") != $tableName) {
        $sql = "CREATE TABLE IF NOT EXISTS $tableName (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            create_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
            payment_type varchar(10) NOT NULL,
            customer varchar(255) NOT NULL,
            paid_date datetime NOT NULL,
            description text NULL,
            total DECIMAL(9,2) SIGNED NOT NULL,
            status varchar(30) NOT NULL,
            remainder DECIMAL(9,2) SIGNED NOT NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        dbDelta($sql);

        add_option('kasa_db_version', $kasa_db_version);
    }
}