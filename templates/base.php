<div id="solokasa">
    <div class="kasaHeader">
        <h1>Kasa <span class="version">v<?php echo $version ?></span></h1>
        <a href="https://solokod.com" target="_blank">solokod.com</a>
    </div>

    <hr>

    <?php
        $adminURL = admin_url('admin-post.php');
        
        if (isset($_GET["module"])) {
            $module = $_GET["module"];

            if ($module == "add") {
                include "add_payment.html";
            } else if ($module == "edit") {
                include "edit_payment.html";
            }
        } else {
            include "dashboard.html";
        }
    ?>
</div>